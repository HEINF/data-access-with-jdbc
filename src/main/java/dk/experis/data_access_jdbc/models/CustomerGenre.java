package dk.experis.data_access_jdbc.models;

import java.util.List;

public record CustomerGenre (Customer customer, List<String> genre) {}
