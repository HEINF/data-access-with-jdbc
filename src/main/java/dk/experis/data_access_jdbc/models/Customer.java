package dk.experis.data_access_jdbc.models;

public class Customer {


    private int id;
    private String firstName;
    private String lastName;
    private String country;
    private String postcode;
    private String phone;
    private String email;

    public Customer(int id, String firstName, String lastName, String country, String postcode, String phone, String email){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postcode = postcode;
        this.phone = phone;
        this.email = email;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Customer id: " + this.id);
        sb.append(", Name: " + this.firstName + " " + this.lastName);
        sb.append(", Country: " + this.country);
        sb.append(", Postcode: " + this.postcode);
        sb.append(", Phone: " + this.phone);
        sb.append(", Email: " + this.email);
        return sb.toString();
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostCode(String postCode) {
        this.postcode = postCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
