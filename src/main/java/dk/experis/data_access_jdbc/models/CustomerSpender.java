package dk.experis.data_access_jdbc.models;

public record CustomerSpender (Customer customer, double totalSpent) {}
