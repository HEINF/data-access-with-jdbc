package dk.experis.data_access_jdbc.models;

public record CustomerCountry (int customerCount, String country) {}
