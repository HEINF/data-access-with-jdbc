package dk.experis.data_access_jdbc.repository;

import dk.experis.data_access_jdbc.models.Customer;
import dk.experis.data_access_jdbc.models.CustomerCountry;
import dk.experis.data_access_jdbc.models.CustomerGenre;
import dk.experis.data_access_jdbc.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements  CustomerRepository{

    private final String url;
    private final String username;
    private final String password;

    //
    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }


    /**
     * Finds all customers in the database.
     * @return List of Customer Objects. Can be empty.
     */
    @Override
    public List<Customer> findAll() {

        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"), result.getString("first_name"),
                        result.getString("last_name"), result.getString("country"), result.getString("postal_code"),
                        result.getString("phone"), result.getString("email")
                );
                customers.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    /**
     * Find a customer by id.
     * @param id used to find Customer Object.
     * @return Customer Object with given id. Can be null.
     */
    @Override
    public Customer findById(Integer id) {

        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"), result.getString("first_name"),
                        result.getString("last_name"), result.getString("country"), result.getString("postal_code"),
                        result.getString("phone"), result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }

    /**
     * Finds customer(s) with a specific name.
     * @param firstName of Customer.
     * @param lastname of Customer.
     * @return List of Customer Objects matching given names. Can be empty.
     */
    @Override
    public List<Customer> findByName(String firstName, String lastname) {

        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer WHERE first_name LIKE ? AND last_name LIKE ?";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, firstName + "%");
            statement.setString(2, lastname + "%");
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"), result.getString("first_name"),
                        result.getString("last_name"), result.getString("country"), result.getString("postal_code"),
                        result.getString("phone"), result.getString("email")
                );
                customers.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    /**
     * Find a section of customers by using a limit and offset.
     * @param limit Number of Customer Object rows to be returned.
     * @param offset Customer Object row to start from (is exclusive of given number).
     * @return List of Customer Objects. Can be empty.
     */
    @Override
    public List<Customer> getPage(int limit, int offset) {

        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer ORDER BY customer_id LIMIT ? OFFSET ?";

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"), result.getString("first_name"),
                        result.getString("last_name"), result.getString("country"), result.getString("postal_code"),
                        result.getString("phone"), result.getString("email")
                );
                customers.add(customer);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    /**
     * Insert a customer into the database.
     * @param customer to be inserted.
     * @return int - number of rows affected by insert.
     */
    @Override
    public int insert(Customer customer) {

        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
        int result = 0;

        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getCountry());
            statement.setString(4, customer.getPostcode());
            statement.setString(5, customer.getPhone());
            statement.setString(6, customer.getEmail());
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Updates a customer.
     * @param customer to be updated.
     * @return int - number of rows affected by the update.
     */
    @Override
    public int update(Customer customer) {

        String sql = "Update customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        int result = 0;

        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.getFirstName());
            statement.setString(2, customer.getLastName());
            statement.setString(3, customer.getCountry());
            statement.setString(4, customer.getPostcode());
            statement.setString(5, customer.getPhone());
            statement.setString(6, customer.getEmail());
            statement.setInt(7, customer.getId());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Not implemented
     * @param object - to be deleted.
     * @return
     */
    @Override
    public int delete(Customer object) {
        return 0;
    }

    /**
     * Not implemented
     * @param id to identify Object to be deleted.
     * @return
     */
    @Override
    public int deleteById(Integer id) {
        return 0;
    }

    /**
     * Find the country with the most customers.
     * @return CustomerCountry Object containing country with most customers and number of customers in that country. Can be null.
     */
    @Override
    public CustomerCountry findMaxCustomerCountry() {

        CustomerCountry customerCountry = null;
        String sql = "SELECT COUNT(customer_id) AS customers, country FROM customer GROUP BY country ORDER BY count(customer_id) DESC LIMIT 1";

        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                customerCountry = new CustomerCountry(resultSet.getInt("customers"), resultSet.getString("country"));
            }
        } catch ( SQLException e) {
            e.printStackTrace();
        }

        return  customerCountry;
    }

    /**
     * Find the customer that has spent the most.
     * @return CustomerSpender Object containing customer who has the largest spent and that customer total spend. Can be null.
     */
    @Override
    public CustomerSpender findMaxSpender() {

        CustomerSpender customerSpender = null;
        String sql = "SELECT SUM(invoice.total) AS spending, customer.* FROM customer " +
                "INNER JOIN invoice ON customer.customer_id = invoice.customer_id " +
                "GROUP BY customer.customer_id ORDER BY spending DESC LIMIT 1"
;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Customer customer = new Customer(
                        resultSet.getInt("customer_id"), resultSet.getString("first_name"),
                        resultSet.getString("last_name"), resultSet.getString("country"),
                        resultSet.getString("postal_code"), resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerSpender = new CustomerSpender(customer, resultSet.getDouble("spending"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  customerSpender;
    }

    /**
     * Find the favourite genre(s) of a customer based on the genres of the tracks they have purchased.
     * @param customer to find favourite genre(s) of.
     * @return CustomerGenre Object containing Customer Object and given customers favourite genre(s). Can be null.
     */
    @Override
    public CustomerGenre findFavouriteGenre(Customer customer){

        CustomerGenre customerGenre = null;
        List<String> genre = new ArrayList<>();
        String sql = "WITH TEMP AS(SELECT genre.name, COUNT(genre.genre_id) AS COUNT FROM customer " +
                "INNER JOIN invoice ON customer.customer_id = invoice.customer_id " +
                "INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id " +
                "INNER JOIN track ON invoice_line.track_id = track.track_id " +
                "INNER JOIN genre ON track.genre_id = genre.genre_id " +
                "WHERE invoice.customer_id = ? GROUP BY genre.genre_id) " +
                "SELECT * FROM TEMP WHERE COUNT = (SELECT MAX(count) FROM TEMP)";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                genre.add(resultSet.getString("name"));
            }
            customerGenre = new CustomerGenre(customer, genre);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerGenre;
    }
}


