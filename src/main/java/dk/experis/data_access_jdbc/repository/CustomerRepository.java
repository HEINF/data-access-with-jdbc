package dk.experis.data_access_jdbc.repository;

import dk.experis.data_access_jdbc.models.Customer;
import dk.experis.data_access_jdbc.models.CustomerCountry;
import dk.experis.data_access_jdbc.models.CustomerGenre;
import dk.experis.data_access_jdbc.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CRUDRepository <Customer, Integer>{
    List<Customer> findByName(String firstName, String lastName);

    List<Customer> getPage(int limit, int offset);

    CustomerCountry findMaxCustomerCountry();

    CustomerSpender findMaxSpender();

    CustomerGenre findFavouriteGenre(Customer customer);

}
