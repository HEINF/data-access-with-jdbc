package dk.experis.data_access_jdbc;

import dk.experis.data_access_jdbc.models.Customer;
import dk.experis.data_access_jdbc.models.CustomerCountry;
import dk.experis.data_access_jdbc.models.CustomerGenre;
import dk.experis.data_access_jdbc.models.CustomerSpender;
import dk.experis.data_access_jdbc.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class ChinookAppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;
    @Autowired
    public ChinookAppRunner(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // Runs all implemented methods in customer repository
        testFindAll();
        testFindById();
        testFindByName();
        testGetPage();
        testInsert();
        testUpdate();
        testFindMaxCustomerCountry();
        testFindMaxSpender();
        testFindFavouriteGenre();

    }

    private void testFindAll() {
        System.out.println("*** Testing findAll() ***");
        List<Customer> customers = customerRepository.findAll();
        customers.forEach(customer -> System.out.println(customer.toString()));
    }

    private void testFindById() {
        System.out.println("*** Testing findById() ***");
        Customer customer = customerRepository.findById((int)(Math.random() * 59) + 1);
        System.out.println(customer.toString());
    }

    private void testFindByName() {
        System.out.println("*** Testing findByName() ***");
        List<Customer> customers = customerRepository.findByName("Kara", "Nielsen");
        customers.forEach(customer -> System.out.println(customer.toString()));
    }

    private void testGetPage() {
        System.out.println("*** Testing GetPage() ***");
        List<Customer> customers = customerRepository.getPage(10, 15);
        customers.forEach(customer -> System.out.println(customer.toString()));
    }

    private void testInsert() {
        System.out.println("*** Testing insert(), may be empty  ***");
        // Check if customer with same name already exists. Customers with the same name are possible, but for test purposes we only insert unique names.
        if (customerRepository.findByName("Clark", "Kent").isEmpty()) {
            Customer customer = new Customer(-1, "Clark", "Kent", "USA", "60629", "(555) 555-1235", "clark.kent@superman.com");
            System.out.println("Rows affected by insert: " + customerRepository.insert(customer));
        }
    }

    private void testUpdate() {
        System.out.println("*** Testing update(), may be empty ***");
        Customer customer = null;
        if (!customerRepository.findByName("Clark", "Kent").isEmpty()) {
            customer = customerRepository.findByName("Clark", "Kent").get(0);
        }
        if(customer != null && !customer.getEmail().equals("clark.kent@definitely-not-superman.com")) {
            System.out.println(customer.getEmail());
            Customer updatedCustomer = new Customer(customer.getId(), "Clark", "Kent", "USA", "60629", "(555) 555-1235", "clark.kent@definitely-not-superman.com");
            System.out.println("Rows affected by update: " + customerRepository.update(updatedCustomer));
        }
    }

    private void testFindMaxCustomerCountry() {
        System.out.println("*** Testing findMaxCustomerCountry() ***");
        CustomerCountry customerCountry = customerRepository.findMaxCustomerCountry();
        System.out.println("Country with most customers: " + customerCountry.country() + ", Number of customers: " + customerCountry.customerCount());
    }

    private void testFindMaxSpender() {
        System.out.println("*** Testing findMaxSpender() ***");
        CustomerSpender customerSpender = customerRepository.findMaxSpender();
        System.out.println(customerSpender.customer().toString() + ", Total spent: " + customerSpender.totalSpent());
    }

    private void testFindFavouriteGenre() {
        System.out.println("*** Testing findFavouriteGenre() ***");
        Customer customer = customerRepository.findById(12);
        CustomerGenre customerGenre = customerRepository.findFavouriteGenre(customer);
        System.out.println(customerGenre.customer().toString());
        System.out.println("Favourite genre(s): ");
        customerGenre.genre().forEach(System.out::println);
    }
}
