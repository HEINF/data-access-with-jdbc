# Data Access With JDBC
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

# Purpose
This project has two learning goals:
- To create scripts to setup and modify a PostgreSQL database.
- To connect to and interact with a PostgreSQL database through a Java Spring application.

## Install
To run the scripts, open PgAdmin and run scripts in Query window.
To run spring application, clone project in IntelliJ or other compatible IDE of your chice. 

## Usage
When run, the Spring application runs a set of test methods.

## Contributing
Not open for contributions.

## License
Unknown
