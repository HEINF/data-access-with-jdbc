DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero (
	id SERIAL PRIMARY KEY,
	name varchar(40) NOT NULL,
	alias varchar(40),
	origin varchar(60)
);

DROP TABLE IF EXISTS assistant;

CREATE TABLE assistant (
	id SERIAL PRIMARY KEY,
	name varchar(40) NOT NULL
);

DROP TABLE IF EXISTS power;

CREATE TABLE power (
	id SERIAL PRIMARY KEY,
	name varchar(40) NOT NULL,
	description varchar(100)
);