INSERT INTO power (name, description) 
VALUES
('Wealth', 'Rich enough to buy superhero technology'),
('X-ray vision', 'Can see through objects'),
('Spider web shooters', 'Shoots spider web that allows grappling and tying down enemies'),
('Punch', 'A powerfull punch');

INSERT INTO superheropower (superhero_id, power_id)
VALUES
(1, 1),
(2, 2),
(3, 3),
(2, 4),
(3, 4);